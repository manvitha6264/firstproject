package com.hcl.inputs;

import java.util.Scanner;

public class IfEsleDemo {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Enter your age:");
    	int age = in.nextInt();
    	if(age>=18) {
    		System.out.println("Your are eligible for voting");
    	}
    	else {
    		System.out.println("Not eligible for voting");
    	}
    }
}
