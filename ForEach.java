package com.hcl.inputs;

public class ForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = {10,20,30,40,50};
		//for each is compressed version of for loop
		for(int x: arr) {
			System.out.println(x);
		}
   
	}

}
