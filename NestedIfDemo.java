package com.hcl.inputs;

import java.util.Scanner;

public class NestedIfDemo {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter Your marks:");
		double marks = in.nextDouble();
		if(marks>35) {
			System.out.println("Congratulations!!You cleared the exam ");
		}
		else if(marks==35) {
			System.out.println("Just passed with deadend");
		}
		else {
			System.out.println("You Failed in the exam!Better luck next time ");
		}
	}

}
